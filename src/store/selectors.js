export const selectorFavorite = (store) => store.favorite.favorites
export const selectorProducts = (store) => store.product.products
export const selectorProductLoading = (store) => store.product.isLoading
export const selectorFavoriteIsModal = (store) => store.favorite.isModal
export const selectorOrderIsModal = (store) => store.order.isModal
export const selectorOrderCount = (store) => store.order.count
export const selectFromData = (state) => state.formData