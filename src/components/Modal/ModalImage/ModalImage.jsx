import PropTypes, { array, object } from 'prop-types'
import styled from "styled-components"
import ModalWrapper from "../ModalWrapper.jsx"
import Modal from "../Modal.jsx"
import ModalHeader from "../ModalHeader.jsx"
import ModalBody from "../ModalBody.jsx"
import ModalFooter from "../ModalFooter.jsx"
import ModalClose from "../ModalClose.jsx"
import {useSelector} from "react-redux";
import {selectorOrderCount} from "../../../store/selectors.js";
import '../Modal.scss'
const PriceSt =styled.span`
  font-size:150%;
  font-weight:400;
  color:  darkgreen;
  padding-top: 1rem;
  font-style:italic;
  `;
 const ArticleSt=styled.p`
 text-align: right;
 font-weight:200;
 font-style:italic;
 `;

const ModalImage = ({products, handleOk, handleClose, isOpen}) =>{
    const handleOutside = (event) => {
        if(!event.target.closest(".modal")){
                handleClose()
        }
    }
    const orderCount = useSelector(selectorOrderCount);
    return(
        <ModalWrapper  className="modal-wrapper" click={handleOutside} isOpen={isOpen}>
            <Modal>
                <ModalClose click={handleClose}/>
                <ModalHeader>
                    <figure>              
                        <img className="card-img-top" src={products.images} alt={products.name} />
                    </figure>
                </ModalHeader>
                <ModalBody>
                    <ArticleSt>{'Артикул: '+ products.article}</ArticleSt>
                    <h3>{products.name}</h3>
                    <p><i>{products.color}</i></p>        
                    <PriceSt>{products.price} грн.</PriceSt>
                </ModalBody>
                <ModalFooter textFirst={!orderCount[products.article] ? "Додати до кошику" : "Видалити з кошика"}
                                clickFirst={handleOk}
                                textSecondary="Закрити"  clickSecondary={handleClose}
                                />
            </Modal>
        </ModalWrapper>
    )
}
ModalImage.defaultProps = {
    handleOk: () => {},
    handleClose: () => {},
  }
ModalImage.propTypes = {
    products: PropTypes.object,
    handleOk: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalImage