import React from 'react';
import {Routes, Route} from "react-router-dom";
import FavoritePage from '../pages/FavoritePage/FavoritePage.jsx';
import CartPage from '../pages/CartPage/CartPage.jsx';
import NotPage from '../pages/NotPage/NotPage.jsx';
import HomePage from '../pages/HomePage/HomePage.jsx';

const AppRoutes = ()  =>{
 
	return (  
		<>
<Routes>
      	<Route path="/" element={<HomePage/>}/>;
		<Route path="*" element={<NotPage/>}/>;
		<Route path="favorite" element={<FavoritePage/>}/>;
		<Route path="order" element={<CartPage/>}/>;
</Routes>
	</>
	)
}
export default AppRoutes

