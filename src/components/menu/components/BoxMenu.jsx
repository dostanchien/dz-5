
import {Link} from "react-router-dom";
import PropTypes from 'prop-types'
import cx from "classnames"
import './Favorite.scss';


const BoxMenu = ({children, title, count, url}) =>{
    return(
        < Link to={url}>
                    <div className="header__actions">
                        <div className="header__favorites-list">
                            {title}      
                            <span className={cx("icon-favorite",(count===0) ? 'text-white' : 'text-red')} >
                                <span className="count">{count}</span>
                            {children}                        
                            </span>
                        </div>
                    </div>
        </Link>
    )
}
BoxMenu.propTypes = {
    children: PropTypes.any,
    title: PropTypes.string,
    url: PropTypes.string,
    count: PropTypes.number,
    onClick: PropTypes.func
}
export default BoxMenu



                  