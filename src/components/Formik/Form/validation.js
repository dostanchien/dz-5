import * as Yup from "yup";

export const validationSchema = Yup.object({
    yuname: Yup.string()
        .matches(/[а-яА-ЯёЁa-zA-Z]$/, 'Не може мати числові значення')
        .min(2, 'Занадто коротке імʼя')
        .max(16, 'Занадто довге імʼя')
        .required('Обовʼязкове поле'),
    surname: Yup.string()
        .matches(/[а-яА-ЯёЁa-zA-Z]$/, 'Не може мати числові значення')
        .min(2, 'Занадто коротке прізвище')
        .max(16, 'Занадто довге прізвище')
        .required('Обовʼязкове поле'),
    year: Yup.string()
        .matches(/^\d{1,3}$/, 'тількі цифри')
        .max(2, 'тількі до двох цифр')
        .test('Після 6', 'Вам має бути більше 6 років', val => val.toString() > 5)
        .test('До 100', 'Вам має бути менше 100 років', val => val.toString() < 100)
        .required('Обовʼязкове поле'),
    delivery: Yup.string()
         .matches(/^[a-zа-яЁёЇїІіЄєҐґ.0-9]/, 'букви и числа')
         .min(2, 'Занадто коротка адреса')
         .max(150, 'Занадто довга адреса')
         .required('Обовʼязкове поле'),
    phone: Yup.string()
        .matches(/^\+380[\s|-]?\d{2}[\s|-]?\d{3}[\s|-]?\d{2}[\s|-]?\d{2}$/, 'невалідний номер')
        .required('Обовʼязкове поле'),
    
})