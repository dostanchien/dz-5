import React from "react";
import { Input, FormItem, SubmitButton, Form } from "formik-antd";
import { Formik } from "formik";
import "antd/dist/antd.css";
import InputMask from "react-input-mask";

function handleValidate(values) {
  let error;
  if (!values.phone) {
    error = {
      phone: "required"
    };
  }
  return error;
}

const CustomInput = props => (
  <InputMask {...props}>{inputProps => <Input {...inputProps} />}</InputMask>
);

const CloseForm = () => (
  <Formik
    initialValues={{ phone: "" }}
    onSubmit={(values, { setSubmitting }) => {
      setTimeout(() => {
        alert(JSON.stringify(values, null, 2));
        setSubmitting(false);
      }, 400);
    }}
    validate={handleValidate}
  >
    {({ isSubmitting, values, handleChange, setFieldValue }) => {
      return (
        <Form>
          <FormItem name="phone" label="Phone" required="true">
            <CustomInput
              mask="+7 (999) 999-99-99"
              name="phone"
              onChange={e => {
                const value = e.target.value || "";
                const changedValue = value
                  .replace(/\)/g, "")
                  .replace(/\(/g, "")
                  .replace(/-/g, "")
                  .replace(/ /g, "");
                console.log({ value });
                console.log({ changedValue });
                setFieldValue("phone", value);
              }}
            />
          </FormItem>
          <SubmitButton type="primary" disabled={isSubmitting}>
            Submit
          </SubmitButton>
          <pre>{JSON.stringify(values, null, 2)}</pre>
        </Form>
      );
    }}
  </Formik>
);

export default CloseForm;
